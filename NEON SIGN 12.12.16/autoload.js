function include(url) {
    var script = document.createElement('script');
    script.src = url;
    document.getElementsByTagName('head')[0].appendChild(script);
}
include("https://cdn.jsdelivr.net/clipboard.js/1.5.12/clipboard.min.js");

$(document).ready(function (e) {

    /** @description: File selection and processing using AJAX */
    $('#fuaifile_c').on('change', function () {
        $('.resp').html('');
        var fd = new FormData();

        fd.append('file', $(this)[0].files[0]);
        /** @description:  AJAX request */
        $.ajax({
            type: 'POST',
            url: '/crm/procfile.php',
            processData: false,
            contentType: false,
            data: fd
            /** @description:  Response with server. Establishing checkboxes after checking   */
        }).done(function (resp) {
            console.log(resp);
            var respon = JSON.parse(resp);
            if (respon.size_pageW == "") {

            } else {
                $('#width_c').val(respon.size_pageW);
                $('#height_c').val(respon.size_pageH);
            }
            if (respon.zoreW == "") {

            } else {
                $('#width_c').val(respon.zoreW);
                $('#height_c').val(respon.zoreH);
            }

            if (respon.rw == "") {

            } else {
                $('#width_c').val(respon.rw);
                $('#height_c').val(respon.rh);
            }


            var textInfoo = respon.textInfo;
            var arr = textInfoo.split("\n");
            if (arr[2] == null) {
                arr[2] = "empty";
            }
            if (arr[1] == null) {
                arr[1] = "empty";
            }
            if (respon) {

                $('.resp').append('<b>' + 'Creation Date: ' + '</b>' + respon.CreationDate + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Creator: ' + '</b>' + respon.Creator + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Mod Date: ' + '</b>' + respon.ModDate + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Producer: ' + '</b>' + respon.Producer + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Title: ' + '</b>' + respon.Title + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Pages: ' + '</b>' + respon.Pages + '<br>' + '<hr>');

//                    $('.resp').append('<b>' + 'Size Page: ' + '</b>' + respon.size_page + '<br>' + '<hr class="hr-line">');
                $('.resp').append('<b>' + 'W: ' + '</b>' + respon.size_pageW + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'H: ' + '</b>' + respon.size_pageH + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'Wt: ' + '</b>' + respon.zoreW + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'Ht: ' + '</b>' + respon.zoreH + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'Text : ' + '</b>' + arr[0] + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Family 1: ' + '</b>' + respon.Font + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'FontH1: ' + '</b>' + respon.FontH + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Weight 1: ' + '</b>' + respon.FontWeight + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'Text2 : ' + '</b>' + arr[1] + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Family 2: ' + '</b>' + respon.Font2 + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'FontH2: ' + '</b>' + respon.FontH1 + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Weight 2: ' + '</b>' + respon.FontWeight1 + '<br>' + '<hr class="hr-line">');

                $('.resp').append('<b>' + 'Text3 : ' + '</b>' + arr[2] + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Family 3: ' + '</b>' + respon.Font3 + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'FontH3: ' + '</b>' + respon.FontH2 + '<br>' + '<hr>');

                $('.resp').append('<b>' + 'Font Weight 3: ' + '</b>' + respon.FontWeight2 + '<br>');
            }
//                        $('.resp').html(resp);

        });

    });
});
