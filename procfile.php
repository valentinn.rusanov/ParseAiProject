<?php


namespace Smalot\PdfParser;

use ReflectionClass;

include './vendor/autoload.php';

/** Description: path to file */
$filename = __DIR__ . '/PDF/' . $_FILES['file']['name'];

$change_name = __DIR__ . '/PDF/' . $_FILES['file']['name'];

/** Description: regular expressions to search the array    */
$size_page = '/[0-9]{1,}x[0-9]{1,}/';
$reg_size_font = '/\d\sTf\n\d{3,}/';
$reg_size_font1 = '/Tj\n\d{3}/';
$reg_size_pageW = '/stDim:w>\d{1,}/';
$reg_size_pageH = '/stDim:h>\d{1,}/';
$size_page1 = '/\d{1,}x/';
$size_page1_1 = '/x/';
$size_page2 = '/x\d{1,}/';

/* @description: Take the temporary file from the tmp */
if (move_uploaded_file($_FILES['file']['tmp_name'], $filename)) {

    preg_match($size_page, $filename, $matches, PREG_OFFSET_CAPTURE, 3);
    preg_match($size_page1, $filename, $matches1);
    preg_match($size_page2, $filename, $matches2);
    $zorex = preg_replace($size_page1_1, "", $matches1);
    $zoran = preg_replace($size_page1_1, "", $matches2);
    $filename = explode('.', $filename);

    rename($change_name, $filename[0] . '.pdf');

    chmod($filename[0] . '.pdf', 0755);

    $parser = new \Smalot\PdfParser\Parser();

    $pdf = $parser->parseFile($filename[0] . '.pdf');


    $fontFam = "";
    $fontWei = "";
    $asd = [];
    $qwe = [];
    global $matches_size;
    global $matches_size1;
    $resum = [];
    $matchesPageW = [];
    $matchesPageH = [];
    /* @description: Retrieve all details from the pdf file. */
    $details = $pdf->getDetails();
    $textInfo = $pdf->getText();
    $fonts = $pdf->getFonts();

    /* @description: The function for the get protected data */
    function accessProtected($obj, $prop)
    {
        $reflection = new ReflectionClass($obj);
        $property = $reflection->getProperty($prop);
        $property->setAccessible(true);
        return $property->getValue($obj);
    }


    /* @Description: selection value in array */
    foreach ($fonts as $key => $font) {
        $docs = accessProtected($font, 'document');

        $obj = accessProtected($docs, 'objects');
        foreach ($obj as $key => $object) {

            $header = accessProtected($object, 'header');

            $content = accessProtected($object, 'content');
            $content1 = accessProtected($object, 'content');
            $content2 = accessProtected($object, 'content');

            $page_size_w = explode("<stDim:", $content);


            preg_match('/\d{1,3}/', $page_size_w[1], $matchesPageWw);
            preg_match('/\d{1,3}/', $page_size_w[2], $matchesPageHh);
            $matchesPageW[] = $matchesPageWw;
            $matchesPageH[] = $matchesPageHh;
            $asdf = explode("TT", $content);

            foreach ($asdf as $loop) {
                if (strlen($loop) != 0) {
                    $asawqer = substr($loop, 7, 3);
                    $preg1 = preg_match('/\d{3}/', $asawqer, $matches_size);
                    if ($preg1) {
                        $resum[] = $matches_size;
                    }
                }

                preg_match($reg_size_font, $loop, $matches_size, PREG_OFFSET_CAPTURE, 3);

            }


            preg_match($reg_size_font1, $content1, $matches_size1, PREG_OFFSET_CAPTURE, 3);


            if ($matches_size[0][0]) {

                preg_match('/\d{3}/', $matches_size[0][0], $asd, PREG_OFFSET_CAPTURE, 3);

            }
            if ($matches_size1[0][0]) {
                preg_match('/\d{3}/', $matches_size1[0][0], $qwe, PREG_OFFSET_CAPTURE, 3);

            }

            $elements = accessProtected($header, 'elements');

            if (isset($elements['FontFamily'])) {
                $fontFam[] = accessProtected($elements['FontFamily'], 'value');

            }

            if (isset($elements['FontWeight'])) {
                $fontWei[] = accessProtected($elements['FontWeight'], 'value');
            }

            if (isset($elements['XHeight'])) {
                $Xheight[] = accessProtected($elements['XHeight'], 'value');

//                print_r($Xheight);
            }

        }

    }

$SH = [];
$SW = [];

    /***
     * @Description: Loop over each property to extract values (string or array).
     */
    /* @Description: selection value in array */
    foreach ($details as $property => $value) {
        preg_match($size_page1, $value, $matches1);
        preg_match($size_page2, $value, $matches2);
        $SW = preg_replace($size_page1_1, "", $matches1);
        $SH = preg_replace($size_page1_1, "", $matches2);

        if(is_array($SW)){
            $SW = implode(', ', $SW);

        }
        $res[$property] = $SW;

        if(is_array($SH)){
            $SH = implode(', ', $SH);

        }
        $res2[$property] = $SH;
        if (is_array($value)) {
            $value = implode(', ', $value);

        }
        $result[$property] = $value;
//         print_r($matches1[0]);
//    echo "<pre>".$property . ' => ' . $value . "\n"."</pre>";

    }
    $rW = $res["Title"];
    $rH = $res2["Title"];
    /** inspection */
    if ($asd[0] == null) {
        $asd[0] = "empty";
    }
    if ($qwe[0] == null) {
        $qwe[0] = "empty";
    }
    if (empty($fontFam[1])) {
        $fontFam[1] = "empty";
    }
    if (empty($fontFam[2])) {
        $fontFam[2] = "empty";
    }
    if ($fontFam[2] == $fontFam[0]) {
        $fontFam[2] = "empty";
    }
    if (empty($matches[0][0])) {
        $matches[0][0] = "empty";
    }
    //FontH
    if (empty($resum[0][0])) {
        $resum[0][0] = "empty";
    }
    if (empty($resum[1][0])) {
        $resum[1][0] = "empty";
    }
    if (empty($resum[2][0])) {
        $resum[2][0] = "empty";
    }

    //FontW
    if (empty($fontWei[1])) {
        $fontWei[1] = "empty";
    }
    if (empty($fontWei[2])) {
        $fontWei[2] = "empty";
    }
    if ($fontWei[2] == $fontWei[0]) {
        $fontWei[2] = "empty";
    }
    /** Record the answers in an array */

    $result["Font"] = $fontFam[0];
    $result["Font2"] = $fontFam[1];
    $result["Font3"] = $fontFam[2];
    $result["FontH"] = $resum[0][0];//$Xheight[0];
    $result["FontH1"] = $resum[1][0]; //$Xheight[1];
    $result["FontH2"] = $resum[2][0];
    $result["FontWeight"] = $fontWei[0];
    $result["FontWeight1"] = $fontWei[1];
    $result["FontWeight2"] = $fontWei[2];
    $result["size_page"] = $matches[0][0];
    $result["size_pageW"] = $matchesPageW[1][0];
    $result["size_pageH"] = $matchesPageH[1][0];
    $result["zoreW"] = $zorex;
    $result["zoreH"] = $zoran;
    $result["SH"] = $SH[0];
    $result["SW"] = $SW[0];
    $result["textInfo"] = $textInfo;
    $result["rw"] = $rW;
    $result["rh"] = $rH;
    $result["aaaaa"] = $filename[0] . '.pdf';
    /* @description:  It returns a JSON-data presentation */
    echo json_encode($result);
//    echo json_encode($res);
//    echo json_encode($res2);
    /* @description: Deleting a file after processing */
    unlink($filename[0] . '.pdf');
}
?>
